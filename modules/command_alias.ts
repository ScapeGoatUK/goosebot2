import * as fs from 'fs';
import { Command } from '../modules/types';
export class AliasHandler {
    private imports: { [index: string]: Command} = {};
    constructor() {
        fs.readdir('../commands', (err, files) => {
            if (err) throw err; 
            else files.forEach(dir => {
                let req = require(dir);
                let init: Command = new req();
                this.imports[init.alias] = init;
            });
        });
    }

    getCommand(alias: string) {
        let command = this.imports[alias];
        if (command != undefined) return this.imports[alias];
    }
}


