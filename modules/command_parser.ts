import { Message, Channel } from 'discord.js';
import { ServerConfigHandler, ServerConfig } from './server_config';

const ServerConfigs = new ServerConfigHandler();

export class CommandParser {

    ParseMessage(msg: Message, callback: Function) {
        if (msg.guild != undefined) {
            let config: ServerConfig = ServerConfigs.getConfig(msg.guild);
            let commandContent: string = '';
            if(config.usePrefixMode && msg.content.startsWith(config.prefix)) {
                commandContent = msg.content.slice(config.prefix.length);
            } else if (msg.channel.id === config.readFromChannelID) {
                commandContent = msg.content;
            }
            callback(commandContent.split(' ').filter(item => item !== ''));
        }
    }
}